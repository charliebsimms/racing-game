
# Requirements
## Overview
The goal is to write a web based “real time” racing game, where a number of cars have to drive a configurable number of 
laps (default 3) around a track. It is a 2 dimensional, top down game and each car is controlled by a user sending 
“commands” that are processed every time interval or turn. The time interval should be configurable but can be set at 1 
second. 
 
## Competitors
When a person joins, he needs to enter his name. He then gets assigned a car (color). A race starts 10 seconds after 
the last person joined, with a minimum of 2 people. A race ends when all cars have either finished or crashed, or 5 
minutes after the leader has finished, or 15 minutes after the start of the race. When the race has ended, the results 
are displayed for 30 seconds, persisted to some file or database and then signup for the next race starts. 
 
## Car controls
The car has three speeds (stopped, slow, fast) and a steering wheel that can either go straight, left or right. In 
every time interval only one command can be received, and a command can only change either the speed or the steering, 
and only change it in one step (so from “stopped” to “slow” or from “left” to “straight” but *not* from “left” to “right” 
or “stopped” to “fast”. The car moves ahead N (N being zero, some number and 2 * some number) meters and might turn left 
or right R degrees (R being ­some number, 0 or +some number). 

## The Track
The track is made out of asphalt, with a strip of grass on both sides and at the end of the grass is a wall. A car can 
only go “fast” on asphalt, on the grass it can either be stopped or going slow. If a car hits the wall, it crashes and 
cannot continue. Cars cannot hit each other. Consider any shape for your track, fine­tune it so some corners have to 
be taken at slow speed. 

The track needs to have a start/finish line, and a grid where the cars line up initially. It probably makes sense to 
divide the track into three sectors to make it easier to detect when a full lap was driven.
 

# Project Documentation
All documentation and project work follow the agile development principals and are held:

    https://www.pivotaltracker.com/n/projects/1575475

# Build, CI and Deployment Environment 
## Definition of Done

The definition of done is the list of criteria that must be complete before any story can be declared finished.

- Feature file defined before the story is started.
- Unit tests pass.
- Automated functional tests that use the feature files.
- Code coverage > 80% for line and conditional coverage.
- Static analysis finds no new issues.
- CI build and cloud deployment successful

## Build, CI and Code quality artifacts
- [Apache Maven 3](https://maven.apache.org/download.cgi)
- [Oracle JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Git version 2](https://git-scm.com/)
- [Jenkins 2](https://jenkins.io/)
- [Sonarqube 5.4](http://www.sonarqube.org/)

## Project Setup
### Clone the Repository
You will need to clone the repo by opening a terminal at a desired directory and executing the following:

    git clone https://charliebsimms@bitbucket.org/charliebsimms/racing-game.git
    
### Build the project
Your working directory should be the racinggame, then you can run the following goal

    mvn clean install
    
## Live Deployment
The game can be played are the following url:
    http://
    